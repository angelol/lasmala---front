# JS-Fullstack - Client

This project is about the epitech module javascript-fullstack

### Requirement

- React
- Redux
- Socket.IO
- Webpack
- Eslint
- Babel

## Installation

1- Download or clone the repo

2- Go into the folder

```bash
yarn install
```

## Command

```bash
npm start // Start client in development mode

npm run build // Build project for production mode

npm run sass // Compile all .scss on .css

npm run lint  // Check errors in code