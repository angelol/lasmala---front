import React from 'react';
import ReactDOM from 'react-dom';
import { SocketProvider } from 'socket.io-react';
import io from 'socket.io-client';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import App from './App';
import store from './store';
import * as serviceWorker from './serviceWorker';
import './assets/styles/css/custom.css';
import './assets/javascripts/custom';
import 'react-toastify/dist/ReactToastify.min.css';

const port = process.env.PORT || 5000;
let socket;
if (process.env.NODE_ENV === 'production') {
	socket = io.connect(`https://${window.location.hostname}?token=${localStorage.getItem('jwtToken')}`);
} else {
	socket = io.connect(`http://localhost:${port}?token=${localStorage.getItem('jwtToken')}`);
}

ReactDOM.render(
	<SocketProvider socket={socket}>
		<Provider store={store}>
			<Router>
				<App />
			</Router>
		</Provider>
		<ToastContainer />
	</SocketProvider>,

	document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
