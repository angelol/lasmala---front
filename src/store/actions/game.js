import axios from 'axios';
import { GET_GAME, GET_ERRORS, GET_SUCCESS } from './types';

export const createGame = history => (dispatch) => {
	axios.post('/api/game/newGame')
		.then((res) => {
			const { game } = res.data;
			history.push(`/game/${game._id}`);
			dispatch({ type: GET_SUCCESS, payload: null });
		})
		.catch(err => dispatch({ type: GET_ERRORS, payload: null }));
};

export const loadGame = gameID => (dispatch) => {
	axios.get(`/api/game/getGame/${gameID}`)
		.then((res) => {
			const { game } = res.data;
			dispatch({
				type: GET_GAME,
				payload: game,
			});
		})
		.catch(err => dispatch({ type: GET_ERRORS, payload: null }));
};

export const submitClick = (x, y, gameID) => (dispatch) => {
	axios.post(`/api/game/submitClick/${gameID}`, {
		posX: x,
		posY: y,
	})
		.then((res) => {
			const { game } = res.data;
			dispatch({
				type: GET_GAME,
				payload: game,
			});
		})
		.catch(err => dispatch({ type: GET_ERRORS, payload: null }));
};
