import axios from 'axios';
import { LIST_USER_ACCOUNT, GET_ERRORS } from './types';

export const getUserAccount = () => (dispatch) => {
	axios.get('/api/superadmin/get/userAccounts')
		.then((res) => {
			const { users } = res.data;
			dispatch({
				type: LIST_USER_ACCOUNT,
				payload: users,
			});
		})
		.catch((err) => {
			dispatch({
				type: GET_ERRORS,
				payload: null,
			});
		});
};

export const getModeratorAccount = () => (dispatch) => {
	axios.get('/api/superadmin/get/moderatorAccounts')
		.then((res) => {
			const { users } = res.data;
			dispatch({
				type: LIST_USER_ACCOUNT,
				payload: users,
			});
		})
		.catch((err) => {
			dispatch({
				type: GET_ERRORS,
				payload: null,
			});
		});
};

export const setRole = (userid, role) => (dispatch) => {
	axios.post('/api/superadmin/set/userRole', { userid, role })
		.catch((err) => {
			dispatch({
				type: GET_ERRORS,
				payload: null,
			});
		});
};
