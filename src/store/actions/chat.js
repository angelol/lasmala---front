import axios from 'axios';
import { SET_CHANNEL, GET_MESSAGES, GET_ERRORS } from './types';

export const setChannel = channel => (dispatch) => {
	localStorage.setItem('subscribeChannel', channel);
	dispatch({
		type: SET_CHANNEL,
		payload: channel,
	});
};

export const getMessages = channel => (dispatch) => {
	axios.get(`/api/chat/getMessages/${channel}`)
		.then((res) => {
			dispatch({
				type: GET_MESSAGES,
				payload: res.data.messages,
			});
		})
		.catch(err => dispatch({ type: GET_ERRORS, payload: null }));
};

export const sendMessage = (message, channel) => (dispatch) => {
	axios.post(`/api/chat/sendMessage/${channel}`, { message })
		.catch((err) => {
			if (err.response.status === 401) {
				dispatch({
					type: GET_ERRORS,
					payload: { message: 'You must be logged' },
				});
			}
		});
};

export const sendInvite = (to, gameId) => (dispatch) => {
	axios.post('/api/chat/sendInvite', { to, gameId })
		.catch((err) => {
			if (err.response.status === 401) {
				dispatch({
					type: GET_ERRORS,
					payload: { message: 'You must be logged' },
				});
			}
		});
};
