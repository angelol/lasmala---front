// GENERAL TYPES
export const GET_ERRORS = 'GET_ERRORS';
export const GET_SUCCESS = 'GET_SUCCESS';
// AUTH TYPES
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const CHECK_AUTH = 'CHECK_AUTH';
export const LOGOUT = 'LOGOUT';

// CHAT TYPES
export const SEND_MESSAGE = 'SEND_MESSAGE';
export const NEW_MESSAGE = 'NEW_MESSAGE';
export const GET_MESSAGES = 'GET_MESSAGES';
export const SET_CHANNEL = 'SET_CHANNEL';

export const FR_CHANNEL = 'FR_CHANNEL';
export const EU_CHANNEL = 'EU_CHANNEL';
export const RU_CHANNEL = 'RU_CHANNEL';
export const EN_CHANNEL = 'EN_CHANNEL';

// INTERFACE
export const UPDATE_ONLINE_COUNTER = 'UPDATE_ONLINE_COUNTER';

// SUPERADMIN
export const LIST_USER_ACCOUNT = 'LIST_USER_ACCOUNT';

// GAME
export const GET_GAME = 'GET_GAME';
