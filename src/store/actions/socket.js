import jwtDecode from 'jwt-decode';
import axios from 'axios';
import {
	NEW_MESSAGE, UPDATE_ONLINE_COUNTER, CHECK_AUTH, LOGOUT, GET_GAME, GET_ERRORS,
} from './types';
import { setCurrentUser } from './authentication';
import setAuthToken from '../../helpers/setAuthToken';

const listen = (socket, history) => (dispatch) => {
	socket.on(NEW_MESSAGE, (data) => {
		dispatch({
			type: NEW_MESSAGE,
			payload: data,
		});
	});

	socket.on(UPDATE_ONLINE_COUNTER, (data) => {
		dispatch({
			type: UPDATE_ONLINE_COUNTER,
			payload: data.payload,
		});
	});

	socket.on(CHECK_AUTH, (data) => {
		if (data.success) {
			const decode = jwtDecode(data.token);
			const user = {
				id: decode.id,
				username: decode.username,
				role: decode.role,
			};
			setAuthToken(data.token);
			dispatch(setCurrentUser(user));
		} else {
			localStorage.removeItem('jwtToken');
			setAuthToken(false);
			dispatch({
				type: LOGOUT,
				payload: null,
			});
		}
	});

	socket.on('PLAYER_JOIN', (data) => {
		dispatch({
			type: GET_GAME,
			payload: data.game,
		});
	});

	socket.on('DISPLAY_WINNER', (data) => {
		if (data.winner === 1) alert('Player 1 WIN');
		else alert('Player 2 WIN');
	});

	socket.on('NEW_INVITE', (data) => {
		if (window.confirm(`You received an invitation to play with ${data.from}`)) {
			axios.post(`/api/game/join/${data.gameId}`)
				.then(() => {
					history.push(`/game/${data.gameId}`);
				})
				.catch(err => dispatch({ type: GET_ERRORS, payload: null }));
		}
	});

	socket.on('UPDATE_GAME', (data) => {
		dispatch({
			type: GET_GAME,
			payload: data.game,
		});
	});

	socket.on('GETCHANNELUSERS', (data) => {
		dispatch({
			type: 'GETCHANNELUSERS',
			payload: data,
		});
	});
};

export default listen;
