import axios from 'axios';
import jwtDecode from 'jwt-decode';
import { toast } from 'react-toastify';
import setAuthToken from '../../helpers/setAuthToken';
import { SET_CURRENT_USER, LOGOUT, GET_ERRORS } from './types';

export const setCurrentUser = decoded => ({
	type: SET_CURRENT_USER,
	payload: decoded,
});


export const login = (user, history) => (dispatch) => {
	axios.post('/api/auth/login', user)
		.then((res) => {
			const { token } = res.data;
			localStorage.setItem('jwtToken', token);
			const decode = jwtDecode(token);
			const User = {
				id: decode.id,
				username: decode.username,
				role: decode.role,
			};

			setAuthToken(token);
			dispatch(setCurrentUser(User));
			history.push('/');
		})
		.catch((err) => {
			toast.error(err.response.data.message);
		});
};

export const register = (user, history) => (dispatch) => {
	axios.post('/api/auth/register', user)
		.then((res) => {
			toast.success(res.data.message);
			setTimeout(() => history.push('/signin'), 2000);
		})
		.catch((err) => {
			toast.error(err.response.data.message);
			dispatch({
				type: GET_ERRORS,
			});
		});
};

export const logoutUser = history => (dispatch) => {
	localStorage.removeItem('jwtToken');
	setAuthToken(false);
	history.push('/signin');
	dispatch({
		type: LOGOUT,
		payload: null,
	});
};
