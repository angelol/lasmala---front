import {
	EU_CHANNEL, SET_CHANNEL, NEW_MESSAGE, GET_MESSAGES,
} from '../actions/types';


const initialState = {
	channel: EU_CHANNEL,
	isLoading: false,
	messages: [],
	listUser: [],
};

export default function (state = initialState, action) {
	switch (action.type) {
	case 'GETCHANNELUSERS': {
		return {
			...state,
			listUser: action.payload,
		};
	}
	case SET_CHANNEL:
		return {
			...state,
			channel: action.payload,
			isLoading: true,
		};
	case GET_MESSAGES:
		return {
			...state,
			messages: action.payload,
			isLoading: false,
		};
	case NEW_MESSAGE:
		return {
			...state,
			messages: [...state.messages, action.payload],
		};
	default:
		return state;
	}
}
