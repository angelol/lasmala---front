import { LIST_USER_ACCOUNT } from '../actions/types';

const initialState = {
	listUser: [],
};

export default function (state = initialState, action) {
	switch (action.type) {
	case LIST_USER_ACCOUNT:
		return {
			...state,
			listUser: action.payload,
		};
	default:
		return state;
	}
}
