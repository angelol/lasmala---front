import { GET_GAME } from '../actions/types';

const initialState = {
	createdBy: {},
	joinedBy: {},
	status: 'PENDING',
	turnPlayer: 1,
	grid: [
		[0, 0, 0],
		[0, 0, 0],
		[0, 0, 0],
	],
};

export default function (state = initialState, action) {
	switch (action.type) {
	case GET_GAME:
		return {
			...state,
			createdBy: action.payload.createdBy,
			joinedBy: action.payload.joinedBy,
			turnPlayer: action.payload.turnPlayer,
			status: action.payload.status,
			grid: action.payload.grid,
		};
	default:
		return state;
	}
}
