import { UPDATE_ONLINE_COUNTER } from '../actions/types';


const initialState = {
	onlineCounter: 0,
};

export default function (state = initialState, action) {
	switch (action.type) {
	case UPDATE_ONLINE_COUNTER:
		return {
			...state,
			onlineCounter: action.payload,
		};
	default:
		return state;
	}
}
