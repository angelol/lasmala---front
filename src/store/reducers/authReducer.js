import { SET_CURRENT_USER, LOGOUT } from '../actions/types';

const initialState = {
	username: '',
	role: '',
	id: '',
};

export default function (state = initialState, action) {
	switch (action.type) {
	case SET_CURRENT_USER:
		return {
			...state,
			username: action.payload.username,
			role: action.payload.role,
			id: action.payload.id,
		};
	case LOGOUT:
		return {
			...state,
			username: '',
			role: ['GUEST'],
			id: '',
		};
	default:
		return state;
	}
}
