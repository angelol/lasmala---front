import { combineReducers } from 'redux';
import errorReducer from './errorReducer';
import authReducer from './authReducer';
import chatReducer from './chatReducer';
import interfaceReducer from './interfaceReducer';
import superadminReducer from './superadminReducer';
import gameReducer from './gameReducer';

export default combineReducers({
	errors: errorReducer,
	auth: authReducer,
	chat: chatReducer,
	interface: interfaceReducer,
	superadmin: superadminReducer,
	game: gameReducer,
});
