import { Permissible } from '@brainhubeu/react-permissible';
import React from 'react';

const higherOrderComponent = (RestrictedComponent, userPermissions, requiredPermissions, callback, oneperm) => {
	const PermissibleComponent = Permissible(
		() => RestrictedComponent,
		userPermissions,
		requiredPermissions,
		callback,
		oneperm,
	);
	class HOC extends React.Component {
		render() {
			if (!userPermissions) {
				return <></>;
			}
			return <PermissibleComponent />;
		}
	}
	return HOC;
};

export default higherOrderComponent;
