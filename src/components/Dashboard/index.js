import React, { Component } from 'react';
import { Route, withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import UserList from './UserList';
import ModeratorList from './ModeratorList';

class Dashboard extends Component {
	render() {
		return (
			<div className="d-flex flex-row">
				<div className="border-right p-2">
					<ul className="nav flex-column">
						<li className="nav-item">
							<Link className="nav-link" to={`${this.props.match.url}/users`}>Users</Link>
							<Link className="nav-link" to={`${this.props.match.url}/moderators`}>Moderators</Link>
						</li>
					</ul>
				</div>
				<div className="container-fluid">
					<Route path={`${this.props.match.path}/users`} component={UserList} />
					<Route path={`${this.props.match.path}/moderators`} component={ModeratorList} />
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	auth: state.auth,
});

export default connect(mapStateToProps)(withRouter(Dashboard));
