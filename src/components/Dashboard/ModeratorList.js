/* eslint-disable no-underscore-dangle */
/* eslint-disable no-restricted-globals */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getModeratorAccount, setRole } from '../../store/actions/superadmin';

const USER_ROLE = 'USER';

class ModeratorList extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			listUser: [],
			prevValue: '',
		};
	}


	handleClick(e) {
		e.preventDefault();
		this.setState({
			prevValue: e.target.value,
		});
	}

	handleChange(e) {
		e.preventDefault();
		if (confirm('Really?')) {
			const index = e.target.selectedIndex;
			const optionElement = e.target.childNodes[index];
			const option = optionElement.getAttribute('data-userid');
			this.props.setRole(option, e.target.value);
		} else {
			e.target.value = this.state.prevValue;
		}
	}

	componentDidMount() {
		this.props.getModeratorAccount();
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			listUser: nextProps.listUser,
		});
	}

	render() {
		const { listUser } = this.state;
		return (
			<table className="table">
				<thead>
					<tr>
						<th>Gender</th>
						<th>Username</th>
						<th>Email</th>
						<th>Role</th>
					</tr>
				</thead>
				<tbody>
					{listUser.map(user => (
						<tr key={user._id}>
							<td>{user.gender}</td>
							<td>{user.username}</td>
							<td>{user.email}</td>
							<td>
								<select className="custom-select" onClick={this.handleClick.bind(this)} onChange={this.handleChange.bind(this)}>
									<option data-userid={user._id} value={user.role}>{user.role}</option>
									<option data-userid={user._id} value={USER_ROLE}>{USER_ROLE}</option>
								</select>

							</td>
						</tr>
					))}
				</tbody>
			</table>
		);
	}
}

ModeratorList.propTypes = {
	getUserAccount: PropTypes.func.isRequired,
	setRole: PropTypes.func.isRequired,
	listUser: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
	listUser: state.superadmin.listUser,
});

export default connect(mapStateToProps, { getModeratorAccount, setRole })(ModeratorList);
