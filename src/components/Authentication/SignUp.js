import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { ValidationForm, TextInput, Radio } from 'react-bootstrap4-form-validation';
import { withLocalize, Translate } from 'react-localize-redux';
import { register } from '../../store/actions/authentication';

class SignUp extends Component {
	constructor(props) {
		super(props);

		this.state = {
			username: '',
			email: '',
			password: '',
			passwordConfirm: '',
			gender: '',
		};
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleInputChange(e) {
		this.setState({
			[e.target.name]: e.target.value,
		});
	}

	handleSubmit(e) {
		e.preventDefault();
		const data = {
			username: this.state.username,
			password: this.state.password,
			passwordConfirm: this.state.passwordConfirm,
			gender: this.state.gender,
			email: this.state.email,
		};
		this.props.register(data, this.props.history);
	}

	render() {
		return (
			<div className="container-fluid">
				<div className="row justify-content-center">
					<div className="col-10 col-sm-8 col-md-6 col-lg-4  shadow rounded p-2">
						<div className="d-flex flex-column align-items-center">
							<h2>{<Translate id="signup" />}</h2>
							<ValidationForm className="" onSubmit={this.handleSubmit}>
								<div className="form-group">
									<Translate>
										{({ translate }) => (
											<TextInput
												name="username"
												autoComplete="username"
												minLength="4"
												maxLength="10"
												value={this.state.username}
												onChange={this.handleInputChange}
												placeholder={`${translate('username')}*`}
												required
											/>
										)
										}
									</Translate>
								</div>
								<div className="form-group">
									<Translate>
										{({ translate }) => (
											<TextInput
												name="email"
												autoComplete="email"
												value={this.state.email}
												onChange={this.handleInputChange}
												placeholder={`${translate('emailAddress')}*`}
												pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
												errorMessage={{ pattern: translate('emailError') }}
												required
											/>
										)
										}
									</Translate>
								</div>
								<div className="form-group">
									<Translate>
										{({ translate }) => (
											<TextInput
												type="password"
												name="password"
												autoComplete="new-password"
												value={this.state.password}
												onChange={this.handleInputChange}
												placeholder={`${translate('password')}*`}
												errorMessage={{ pattern: translate('passwordError') }}
												pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$"
												required
											/>
										)
										}
									</Translate>
								</div>
								<div className="form-group">
									<Translate>
										{({ translate }) => (
											<TextInput
												type="password"
												autoComplete="new-password"
												name="passwordConfirm"
												value={this.state.passwordConfirm}
												onChange={this.handleInputChange}
												placeholder={`${translate('passwordConfirm')}*`}
												pattern={this.state.password}
												errorMessage={{ pattern: translate('passwordConfirmError') }}
												required
											/>
										)
										}
									</Translate>

								</div>
								<div className="form-group d-flex flex-row justify-content-around">
									<label><Translate id="gender" /></label>
									<Translate>
										{({ translate }) => (
											<Radio.RadioGroup
												name="gender"
												required
												errorMessage="Please select gender"
												valueSelected={this.state.gender}
												onChange={this.handleInputChange}
											>
												<Radio.RadioItem id="male" label={`${translate('male')}`} value="male" />
												<Radio.RadioItem id="female" label={`${translate('female')}`} value="female" />
											</Radio.RadioGroup>
										)}
									</Translate>
								</div>
								<div className="form-group">
									<button type="submit" className="btn btn-primary w-100">
										<Translate id="signup" />
									</button>
								</div>
								<div className="form-group">
									<Link to="/signin">
										<Translate id="haveAccount" />
									</Link>
								</div>
							</ValidationForm>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	auth: state.auth,
});


SignUp.propTypes = {
	auth: PropTypes.object.isRequired,
	register: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { register })(withRouter(withLocalize(SignUp)));
