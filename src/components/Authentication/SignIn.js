import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withLocalize, Translate } from 'react-localize-redux';
import globalTranslations from '../../translations/global.json';
import { login } from '../../store/actions/authentication';


class SignIn extends Component {
	constructor(props) {
		super(props);

		this.state = {
			username: '',
			password: '',
		};
		this.props.addTranslation(globalTranslations);

		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleInputChange(e) {
		this.setState({
			[e.target.name]: e.target.value,
		});
	}

	handleSubmit(e) {
		e.preventDefault();
		const data = {
			username: this.state.username,
			password: this.state.password,
		};
		this.props.login(data, this.props.history);
	}


	render() {
		return (
			<div className="container-fluid">
				<div className="row justify-content-center">
					<div className="col-10 col-sm-8 col-md-6 col-lg-4  shadow rounded p-2">
						<div className="d-flex flex-column align-items-center">
							<h2><Translate id="signin" /></h2>
							<form onSubmit={this.handleSubmit}>
								<div className="form-group">
									<Translate>
										{({ translate }) => (
											<input
												type="text"
												autoComplete="username"
												className="form-control"
												name="username"
												value={this.state.username}
												onChange={this.handleInputChange}
												placeholder={`${translate('username')}*`}
												required
											/>
										)
										}
									</Translate>
								</div>
								<div className="form-group">
									<Translate>
										{({ translate }) => (
											<input
												type="password"
												autoComplete="current-password"
												className="form-control"
												name="password"
												value={this.state.password}
												onChange={this.handleInputChange}
												placeholder={`${translate('password')}*`}
												required
											/>
										)
										}
									</Translate>
								</div>
								<div className="form-group">
									<button type="submit" className="btn btn-primary w-100">
										<Translate id="signin" />
									</button>
								</div>
								<div className="d-flex flex-row">
									<p><Translate id="noAccount" /></p>
&nbsp;&nbsp;
									<Link to="/signup">
										<Translate id="signupNow" />
									</Link>
								</div>
								<div className="form-group">
									<Link to="#">
										<Translate id="forgotPassword" />
									</Link>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	auth: state.auth,
});

SignIn.propTypes = {
	auth: PropTypes.object.isRequired,
	login: PropTypes.func.isRequired,
};


export default connect(mapStateToProps, { login })(withRouter(withLocalize(SignIn)));
