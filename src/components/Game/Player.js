import React from 'react';

const Player = (props) => {
	if (props.player) {
		return (
			<div className="d-flex flex-column">
				<h3>Player {props.number}</h3>
				<h4 className="text-primary">{props.player.username}</h4>
				<h5>Score : {props.player.win}</h5>
			</div>
		);
	}
	return (null);
};

export default Player;
