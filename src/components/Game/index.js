/* eslint-disable no-nested-ternary */
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { socketConnect } from 'socket.io-react';
import { loadGame } from '../../store/actions/game';
import Grid from './Grid';
import Player from './Player';

class Game extends React.Component {
	state = {
		createdBy: {},
		joinedBy: {},
		status: '',
		turnPlayer: 0,
		grid: [],
	}


	componentDidMount() {
		this.props.loadGame(this.props.match.params.id);
		this.props.socket.emit('subscribeGame', this.props.match.params.id);
		localStorage.setItem('currentGameId', this.props.match.params.id);
	}

	componentWillUnmount() {
		localStorage.removeItem('currentGameId');
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			createdBy: nextProps.game.createdBy,
			joinedBy: nextProps.game.joinedBy,
			grid: nextProps.game.grid,
			status: nextProps.game.status,
			turnPlayer: nextProps.game.turnPlayer,
		});
	}

	render() {
		return (
			<div className="container">
				<div className="d-flex flex-row justify-content-center">
					{ this.state.status === 'PENDING'
						? <h3>Waiting for a second player</h3>
						: this.state.turnPlayer === 1
							? <h3>Player 1 is playing</h3> : <h3>Player 2 is playing</h3>
					}
				</div>
				<div className="d-flex flex-row justify-content-between">
					<div className="player1 d-flex flex-column justify-content-start">
						<Player player={this.state.createdBy} number={1} />
					</div>
					<div className="player2  d-flex flex-column justify-content-end">
						<Player player={this.state.joinedBy} number={2} />
					</div>
				</div>
				<Grid grid={this.state.grid}/>
			</div>
		);
	}
}
Game.propTypes = {
	game: PropTypes.object.isRequired,
	loadGame: PropTypes.func.isRequired,
};
const mapStateToProps = state => ({
	game: state.game,
});
export default connect(mapStateToProps, { loadGame })(withRouter(socketConnect(Game)));
