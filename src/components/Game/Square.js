import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { submitClick } from '../../store/actions/game';

class Square extends React.Component {
	handleSquareClick(e) {
		const { posx, posy } = e.currentTarget.dataset;
		this.props.submitClick(posx, posy, this.props.match.params.id);
	}

	render() {
		const { value, posX, posY } = this.props;
		if (Number(value) === 0) {
			return (
				<div className="border square square-blank" data-posx={posX} data-posy={posY} onClick={e => this.handleSquareClick(e)} />
			);
		}
		if (Number(value) === 1) {
			return (
				<div className="border d-flex justify-content-center align-items-center square square-p1" />
			);
		}
		if (Number(value) === -1) {
			return (
				<div className="border d-flex justify-content-center align-items-center square square-p2" />
			);
		}
		return (null);
	}
}

Square.propTypes = {
	submitClick: PropTypes.func.isRequired,
};


export default connect(null, { submitClick })(withRouter(Square));
