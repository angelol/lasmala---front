import React from 'react';
import Square from './Square';

const Grid = (props) => {
	const { grid } = props;
	if (grid.length <= 0) {
		return null;
	}
	return (
		<div className="d-flex flex-column align-items-center">
			<div className="d-flex flex-row">
				<Square value={grid[0][0]} posX={0} posY={0} />
				<Square value={grid[0][1]} posX={0} posY={1} />
				<Square value={grid[0][2]} posX={0} posY={2} />
			</div>
			<div className="d-flex flex-row">
				<Square value={grid[1][0]} posX={1} posY={0} />
				<Square value={grid[1][1]} posX={1} posY={1} />
				<Square value={grid[1][2]} posX={1} posY={2} />
			</div>
			<div className="d-flex flex-row">
				<Square value={grid[2][0]} posX={2} posY={0} />
				<Square value={grid[2][1]} posX={2} posY={1} />
				<Square value={grid[2][2]} posX={2} posY={2} />
			</div>
		</div>
	);
};

export default Grid;
