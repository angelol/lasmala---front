import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { withLocalize, Translate } from 'react-localize-redux';
import { createGame } from '../store/actions/game';
import globalTranslations from '../translations/global.json';


class Home extends React.Component {
	handleCreate(e) {
		e.preventDefault();
		this.props.createGame(this.props.history);
	}

	render() {
		return (
			<div className="jumbotron d-flex flex-column align-items-center shadow">
				<h2><Translate id="welcome" /></h2>
				<ul className="list-group shadow">
					<li className="list-group-item text-center">
						<h3><Translate id="howPLay" /></h3>
					</li>
					<li className="list-group-item text-center">
						<h4><Translate id="step1" /></h4>
					</li>
					<li className="list-group-item text-center">
						<h4><Translate id="step2" /></h4>
					</li>
					<li className="list-group-item text-center">
						<h4><Translate id="step3" /></h4>
					</li>
					<li className="list-group-item text-center">
						<h4><Translate id="step4" /></h4>
					</li>
					<li className="list-group-item text-center">
						<h4><Translate id="step5" /></h4>
					</li>
					<li className="list-group-item text-center">
						<h4><Translate id="step6" /></h4>
					</li>
					<li className="list-group-item text-center">
						<h4><Translate id="step7" /> 😀</h4>
					</li>
					<li className="list-group-item text-center">
						<button className="btn btn-lg btn-outline-primary" onClick={e => this.handleCreate(e)}>
							Create game
						</button>
					</li>
				</ul>
			</div>
		);
	}
}

export default connect(null, { createGame })(withRouter(withLocalize(Home)));
