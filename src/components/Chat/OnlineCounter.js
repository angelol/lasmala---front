import { connect } from 'react-redux';

const OnlineCounter = props => (
	props.onlineCounter
);

const mapStateToProps = state => ({
	onlineCounter: state.interface.onlineCounter,
});

export default connect(mapStateToProps)(OnlineCounter);
