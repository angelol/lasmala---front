import React from 'react';

const ListUser = (props) => {
	if (props.users && props.users.length > 0) {
		return (
			<div className="chatUserList d-flex flex-column">
				{props.users.map(user => (
					<div className="item d-flex flex-row align-items-center mt-1" key={user} data-user={user} onClick={e => props.handleSelectUser(e)}>
						<img src={`/avatar/${user}.png`} alt={user} />
						<span>{user}</span>
					</div>
				))}
			</div>
		);
	}
	return <></>;
};

export default ListUser;
