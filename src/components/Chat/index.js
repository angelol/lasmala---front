import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { socketConnect } from 'socket.io-react';
import { PermissibleRender } from '@brainhubeu/react-permissible';
import { PulseLoader } from 'halogenium';
import {
	setChannel, getMessages, sendMessage, sendInvite,
} from '../../store/actions/chat';
import {
	FR_CHANNEL, EU_CHANNEL, RU_CHANNEL, EN_CHANNEL,
} from '../../store/actions/types';
import store from '../../store';
import OnlineCounter from './OnlineCounter';
import InputGuest from './InputGuest';
import InputLogged from './InputLogged';
import ListUser from './ListUser';

if (localStorage.subscribeChannel) {
	store.dispatch(setChannel(localStorage.subscribeChannel));
}

const getColor = (role) => {
	switch (role) {
	case 'SUPERADMIN':
		return 'red';
	case 'MODERATOR':
		return 'green';
	default:
		return 'gray';
	}
};

const Loading = (props) => {
	if (!props.isLoading) {
		return <>
			{props.messages.map((msg, index) => (
				<div key={index} className="items">
					<div className="d-flex flex-row justify-content-between">
						<span className="User" style={{ fontSize: '12px', fontWeight: '700', color: getColor(msg.userRole) }}>{msg.username}</span>
					</div>
					<span className="Body">{msg.body}</span>
				</div>
			))}
		</>;
	}
	return <PulseLoader className="h-100 d-flex justify-content-center align-items-center" color="#26A65B" size="16px" margin="4px" />;
};


class Chat extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isLoading: false,
			channel: '',
			message: '',
			messages: [],
			listUser: [],
		};
	}

	handleSelectUser(e) {
		this.setState({
			message: `@${e.currentTarget.dataset.user}`,
			listUser: [],
		});
	}

	handleSubmit(e) {
		e.preventDefault();
		if (this.state.message[0] === '@') {
			const gameId = localStorage.getItem('currentGameId');
			if (gameId) {
				this.props.sendInvite(this.state.message.split(' ')[0].slice(1), gameId);
			}
		} else {
			this.props.sendMessage(this.state.message, this.state.channel);
		}
		this.setState({
			message: '',
		});
	}

	handleInputChange(e) {
		this.setState({
			[e.target.name]: e.target.value,
		}, () => {
			if (this.state.message === '@') {
				this.props.socket.emit('getChannelUsers', this.state.channel);
			} else if (this.state.message === '') {
				this.setState({
					listUser: [],
				});
			}
		});
	}

	handleSetChannel(e) {
		e.preventDefault();
		if (e.target.name !== this.state.channel) {
			this.setState({
				channel: e.target.name,
			}, () => {
				this.props.setChannel(this.state.channel);
				this.props.socket.emit('subscribeChannel', this.state.channel);
				this.props.getMessages(this.state.channel);
			});
		}
	}


	componentDidMount() {
		this.setState({
			channel: this.props.chat.channel,
		}, () => {
			this.props.socket.emit('subscribeChannel', this.state.channel);
			this.props.getMessages(this.state.channel);
		});
	}


	componentWillReceiveProps(nextProps) {
		if (nextProps.chat.messages) {
			this.setState({
				messages: nextProps.chat.messages,
				isLoading: nextProps.chat.isLoading,
			});
		}
		if (nextProps.chat.listUser) {
			this.setState({
				listUser: nextProps.chat.listUser,
			});
		}
	}


	render() {
		const { user } = this.props;
		return (
		<>
			<nav className="sidebar flex-column justify-content-between align-items-center p-2">
				<div className="header d-flex flex-row justify-content-between align-items-center">
					<span className="onlineCounter">
						<OnlineCounter /> {' '} online
					</span>
					<div className="dismiss text-primary">
						<i className="fas fa-times" />
					</div>
				</div>
				<div className="content border border-primary rounded d-flex flex-column align-items-left">
					<Loading isLoading={this.state.isLoading} messages={this.state.messages} />
					<ListUser users={this.state.listUser} handleSelectUser={e => this.handleSelectUser(e)} />
				</div>
				<div className="footer d-flex flex-column">
					<PermissibleRender
						userPermissions={user.role}
						requiredPermissions={['USER', 'MODERATOR', 'SUPERADMIN']}
						renderOtherwise={<InputGuest />}
						oneperm
					>
						<InputLogged
							handleSubmit={e => this.handleSubmit(e)}
							handleInputChange={e => this.handleInputChange(e)}
							message={this.state.message}
						/>
					</PermissibleRender>
					<div className="d-flex flex-row align-items-center">
						<img src="/images/fr-flag.png" name={FR_CHANNEL} onClick={e => this.handleSetChannel(e)} alt="" className="btn-channel mr-2" />
						<img src="/images/eu-flag.png" name={EU_CHANNEL} onClick={e => this.handleSetChannel(e)} alt="" className="btn-channel mr-2" />
						<img src="/images/ru-flag.png" name={RU_CHANNEL} onClick={e => this.handleSetChannel(e)} alt="" className="btn-channel mr-2" />
						<img src="/images/en-flag.png" name={EN_CHANNEL} onClick={e => this.handleSetChannel(e)} alt="" className="btn-channel" />
					</div>
				</div>

			</nav>
			<button type="button" id="sidebarCollapse" className="btn btn-lg btn-outline-info d-flex align-items-center justify-content-center">
				<i className="fas fa-comments" />
			</button>
			</>
		);
	}
}

const mapStateToProps = state => ({
	chat: state.chat,
	user: state.auth,
});

const mapDispatchToProps = dispatch => ({
	sendMessage: (message, channel) => dispatch(sendMessage(message, channel)),
	setChannel: channel => dispatch(setChannel(channel)),
	getMessages: channel => dispatch(getMessages(channel)),
	sendInvite: (to, gameId) => dispatch(sendInvite(to, gameId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(socketConnect(Chat)));
