import React from 'react';

const InputLogged = props => (
	<form onSubmit={props.handleSubmit}>
		<div className="input-group">
			<input
				type="text"
				className="form-control border-primary"
				placeholder="Write a message..."
				aria-label="Write a message..."
				onChange={props.handleInputChange}
				value={props.message}
				name="message"
				autoComplete="off"
			/>
			<div className="input-group-append">
				<button type="submit" className="btn btn-outline-primary">
					<i className="fas fa-share" />
				</button>
			</div>
		</div>
	</form>
);

export default InputLogged;
