import React from 'react';

const InputGuest = () => (
	<form>
		<div className="input-group">
			<input
				type="text"
				className="form-control border-primary"
				placeholder="Login to write a message..."
				aria-label="Login to write a message..."
				name="message"
				disabled
			/>
			<div className="input-group-append">
				<button type="submit" className="btn btn-outline-primary" disabled>
					<i className="fas fa-share" />
				</button>
			</div>
		</div>
	</form>
);

export default InputGuest;
