import React from 'react';
import { Link } from 'react-router-dom';
import { Translate } from 'react-localize-redux';

const GuestLink = () => <>
	<li className="nav-item">
		<Link className="nav-link  mr-sm-2" to="/signin">
			<Translate id="signin" />
		</Link>
	</li>
	<li className="nav-item">
		<Link className="nav-link text-primary  mr-sm-2 " to="/signup">
			<Translate id="signup" />
		</Link>
	</li>
</>;

export default GuestLink;
