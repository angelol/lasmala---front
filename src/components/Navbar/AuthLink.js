import React from 'react';
import { Link } from 'react-router-dom';
import { Translate } from 'react-localize-redux';
import { PermissibleRender } from '@brainhubeu/react-permissible';


function AuthLinks(props) {
	return (
		<>
			<li className="nav-item dropdown">
				<Link className="nav-link dropdown-toggle d-flex flex-row align-items-center" to="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<img src={`/avatar/${props.user.username}.png`} height="50" width="50" className="border rounded-circle shadow mr-2 p-1" alt="" />
					{props.user.username}
				</Link>
				<div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

					<PermissibleRender
						userPermissions={props.user.role}
						requiredPermissions={['SUPERADMIN']}
					>
						<Link to="/dashboard" className="dropdown-item">
							<Translate id="dashboard" />
						</Link>
					</PermissibleRender>

					<Link className="dropdown-item" to="#" onClick={props.logout}>
						<Translate id="logout" />
					</Link>
				</div>
			</li>
		</>
	);
}

export default AuthLinks;
