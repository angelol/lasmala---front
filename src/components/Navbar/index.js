import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { PermissibleRender } from '@brainhubeu/react-permissible';
import { withLocalize } from 'react-localize-redux';
import { logoutUser } from '../../store/actions/authentication';
import Translation from '../../translations';

import globalTranslations from '../../translations/global.json';

import GuestLink from './GuestLink';
import AuthLink from './AuthLink';

class Navbar extends Component {
	constructor(props) {
		super(props);
		this.props.addTranslation(globalTranslations);
	}

	onLogout(e) {
		e.preventDefault();
		this.props.logoutUser(this.props.history);
	}

	render() {
		const { user } = this.props;
		return (
			<nav className="navbar navbar-expand-lg navbar-light bg-light shadow-sm mb-5">
				<Link className="navbar-brand mb-0 h1" to='/'>JS-Fullstack</Link>
				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					<span className="navbar-toggler-icon" />
				</button>

				<div className="collapse navbar-collapse" id="navbarNav">
					<ul className="navbar-nav ml-auto">
						<form class="form-inline">
							<Translation/>
						</form>
						<PermissibleRender
							userPermissions={user.role}
							requiredPermissions={['USER', 'MODERATOR', 'SUPERADMIN']}
							renderOtherwise={<GuestLink />}
							oneperm
						>
							<AuthLink user={user} logout={this.onLogout.bind(this)} />
						</PermissibleRender>
					</ul>
				</div>
			</nav>
		);
	}
}

Navbar.propTypes = {
	logoutUser: PropTypes.func.isRequired,
	user: PropTypes.object.isRequired,
};


const mapStateToProps = state => ({
	user: state.auth,
});


export default connect(mapStateToProps, { logoutUser })(withRouter(withLocalize(Navbar)));
