import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { withLocalize } from 'react-localize-redux';
import globalTranslations from './global.json';
import LanguageToggle from './LanguageToggle';

class Main extends React.Component {
	constructor(props) {
		super(props);

		const languages = [
			{ name: 'English', code: 'en' },
			{ name: 'French', code: 'fr' },
		];
		const defaultLanguage = localStorage.getItem('languageCode') || languages[0].code;
		const { initialize } = this.props;
		initialize({
			languages,
			translation: globalTranslations,
			options: {
				renderToStaticMarkup,
				defaultLanguage,
			},
		});
	}

	componentDidUpdate(prevProps) {
		const prevLangCode = prevProps.activeLanguage && prevProps.activeLanguage.code;
		const curLangCode = this.props.activeLanguage && this.props.activeLanguage.code;

		const hasLanguageChanged = prevLangCode !== curLangCode;

		if (hasLanguageChanged) {
			localStorage.setItem('languageCode', curLangCode);
		}
	}

	render() {
		return (
			<LanguageToggle />
		);
	}
}

export default withLocalize(Main);
