import React from 'react';
import PropTypes from 'prop-types';
import { withLocalize } from 'react-localize-redux';

const LanguageToggle = ({ languages, activeLanguage, setActiveLanguage }) => (
	<>
		{languages.map(lang => (
			<li className="nav-item" key={lang.code}>
				<button key={lang.code} type="button" className="btn btn-outline-primary my-2 my-sm-0 mr-2" onClick={() => setActiveLanguage(lang.code)}>
					{lang.name}
				</button>
			</li>
		))}
	</>
);
export default withLocalize(LanguageToggle);
