/* eslint-disable no-undef */
$(document).ready(() => {
	$('#sidebarCollapse').on('click', () => {
		$('.overlay, .sidebar').toggleClass('active');
	});

	$('.overlay, .sidebar > .header > .dismiss').on('click', () => {
		$('.overlay, .sidebar').toggleClass('active');
	});
});
