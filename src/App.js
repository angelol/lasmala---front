/* eslint-disable @exact/reactive/import-exact-antd */
/* eslint-disable no-shadow */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, withRouter, Switch } from 'react-router-dom';
import { LocalizeProvider } from 'react-localize-redux';
import { socketConnect } from 'socket.io-react';
import listen from './store/actions/socket';

import Chat from './components/Chat/index';
import Game from './components/Game/index';
import Home from './components/Home';
import Navbar from './components/Navbar/index';
import SignUp from './components/Authentication/SignUp';
import SignIn from './components/Authentication/SignIn';
import Dashboard from './components/Dashboard';
import Permission from './components/HOC/Permission';

const Error = () => (
	<div> Sorry, this page does not exist. </div>
);

class App extends Component {
	componentDidMount() {
		const { listen, socket, history } = this.props;
		listen(socket, history);
	}

	render() {
		const { user, history } = this.props;
		return (
			<LocalizeProvider>
          <>
            <Chat />
            <Navbar />
            {/* <Translation /> */}
			<div className="App container">
				<Switch>
					<Route exact path="/" component={Home} />
					<Route path="/game/:id" component={Permission(<Game />, user.role, ['USER', 'SUPERADMIN', 'MODERATOR'], () => { history.push('/signin'); }, true)} />
					<Route path="/signin" component={Permission(<SignIn />, user.role, ['GUEST'], () => { history.push('/'); }, false)} />
					<Route path="/signup" component={Permission(<SignUp />, user.role, ['GUEST'], () => { history.push('/'); }, false)} />
					<Route path="/dashboard" component={Permission(<Dashboard />, user.role, ['SUPERADMIN'], null, false)} />
					<Route component={Error} />
				</Switch>
			</div>
            <div className="overlay" />
          </>
			</LocalizeProvider>
		);
	}
}

const mapStateToProps = state => ({
	user: state.auth,
});


export default withRouter(connect(mapStateToProps, { listen })(socketConnect(App)));
