import jwtDecode from 'jwt-decode';

export const isLogged = () => !!localStorage.getItem('jwtToken');

export const isRole = (role) => {
	const token = localStorage.getItem('jwtToken');
	if (token) {
		const decoded = jwtDecode(token);
		if (decoded.role.indexOf(role) !== -1) {
			return true;
		}
	}
	return false;
};
// export default isLogged;
